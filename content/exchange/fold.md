---
title: BTC_DCA - Fold
date: 2021-02-14
subtitle: Fold
---

Visit [foldapp.com](https://use.foldapp.com/r/Ul3zJuaI) (get 5000 sats when you sign up from this affiliate link)

### Advantages of Using Fold App
  - No KYC for gift card purchases (only for debit card)
  - Bitcoin rewards from everyday USD Visa debit purchases
  - Fun gamification

### Shortcomings of Using Fold App
  - No ability to directly buy/sell BTC
  - Debit card has annual fee (do the math to determine if it's worth it for you)

### Conclusion about using Fold App
  - Good option to augment direct purchases
