---
title: BTC_DCA - River
date: 2021-02-14
subtitle: River
---

Visit [river.com](https://river.com)

### Advantages of Using River Financial
  - Hardware wallet integration
  - Mobile app
  - Ability to sell BTC for USD

### Shortcomings of Using River Financial
  - High minimum limit ($100)
  - No auto withdrawal
  - Available in only 31 states ([ref](https://river.com/support/knowledge-base/articles/account-eligibility-criteria-and-requirements))

### Conclusion about River Financial
  - Very polished interface, promising roadmap
