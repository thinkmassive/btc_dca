---
title: BTC_DCA - Square (Cash App)
date: 2021-02-14
subtitle: Square (Cash App)
---

Visit [cash.app/bitcoin](https://cash.app/bitcoin)

### Advantages of Using Cash App
  - Short delay from purchase to self-custody
  - Mobile app
  - Low minimum limit ($10 for recurring ([ref](https://cash.app/help/us/en-us/3109-schedule-automatic-purchases))

### Shortcomings of Using Cash App
  - No auto-withdrawal
  - Must first deposit USD to your Cash App balance (can also be automated)

### Conclusion about Cash App
  - Best rates for small purchases
  - Good choice for existing users of the app (avoids exposing KYC elsewhere)
