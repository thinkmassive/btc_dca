---
title: BTC_DCA - Swan
date: 2021-02-14
subtitle: Swan
---

Visit [swanbitcoin.com](https://www.swanbitcoin.com/thinkmassive/) (get $10 of BTC when you sign up w/this affiliate link)

### Advantages of Using Swan Bitcoin
  - Best overall rate (w/annual fees paid up front)
  - Low minimum limit ($10)
  - Auto-withdrawal option
  - Available in all 50 states ([ref](https://help.swanbitcoin.com/hc/en-us/articles/360059000753-Is-Swan-Bitcoin-available-in-all-50-states-))

### Shortcomings of Using Swan Bitcoin
  - Auto-withdrawal re-uses addresses
  - Long delay from purchase to self-custody
  - No ability to sell (buy-only)

### Conclusion about Swan Bitcoin
  - Best overall rate (w/annual fees paid up front)
  - Best choice for restrictive states
