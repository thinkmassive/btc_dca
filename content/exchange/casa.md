---
title: BTC_DCA - Casa
date: 2021-02-14
subtitle: Casa
---

Visit [keys.casa](https://keys.casa)

### Advantages of Using Casa Buy BTC
  - Instant purchase to self-custody
  - Mobile app (Buy BTC integrated w/personal key manager)
  - Low minimum limit ($20)

### Shortcomings of Using Casa Buy BTC
  - High miner fee (mandatory)
  - ACH available in only 43 states ([ref](https://support.keys.casa/hc/en-us/articles/360047119631-Supported-Regions-for-Buying-Bitcoin))

### Conclusion about Casa Buy BTC
  - Best choice for a fully integrated self-custody solution
  - Very good rates if ACH is available in your state
