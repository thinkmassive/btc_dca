---
title: About BTC DCA .info
date: 2021-02-13T20:54:00-05:00
draft: false
comments: false
---

## Criteria
  - Recurring USD to BTC exchange to customers residing in United States
  - Rates checked within a 5-minute window of noon EST each Saturday

---

## Attribution

Thank you to the following artists from <a href="https://pixabay.com">Pixabay</a>:

  - [WorldSpectrum](https://pixabay.com/users/worldspectrum-7691421)  (background & account icons)
  - [WikimediaImages](https://pixabay.com/users/wikimediaimages-1185597) (Bitcoin logo)

---

## Contact

Please use the following channels for updates or corrections:

  - Gitlab: [thinkmassive/btc_usd](https://gitlab.com/thinkmassive/btc_usd)
  - Email: alex at thinkmassive dot org
  - Fediverse: [@btc_dca@ctdl.co](https://ctdl.co/@btc_dca)
  - Twitter: [@btc_dca](https://twitter.com/btc_dca)
